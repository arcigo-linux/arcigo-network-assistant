#!/bin/bash
interface=$(hwinfo --netcard | grep "Device File" | cut -d " " -f5 | grep e)

if [[ $(ethtool $interface | grep Wake-on | awk '!/Supports Wake-on/' | awk '/Wake-on/ { print $NF }') == "g" ]] ; then 
echo "enabled"
else
echo "disabled"
fi
