 #!/bin/bash
interface=$(hwinfo --wlan | grep "Device File" | cut -d " " -f5)

if [[ $(iw dev $interface get power_save | awk '/Power save/ { print $NF }') == "on" ]]; then
echo "enabled"
elif [[ $(iwconfig $interface | grep -A1 Power | awk '/Power/ { print $NF }') == "Management:on" ]]; then
echo "enabled"
else
echo "disabled"
fi
 
